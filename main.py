import json
import typing
import unittest

import pygame
import random
from typing import Optional
from enum import Enum

WINDOW_DIMENSIONS = (900, 900)
TILE_NUMBER = 40
TILE_SIZE = WINDOW_DIMENSIONS[0] // TILE_NUMBER


class Side(Enum):
    NORTH = "north"
    EAST = "east"
    SOUTH = "south"
    WEST = "west"


class Position:
    def __init__(self, position: (int, int)):
        self.x = position[0]
        self.y = position[1]

    def __repr__(self):
        return f"({self.x}, {self.y})"

    def __eq__(self, other):
        if not isinstance(other, Position):
            return False

        return self.x == other.x and self.y == other.y

    def get_position(self) -> (int, int):
        return self.x, self.y


def clamp(dimension: int, position: (int, int)) -> Optional[Position]:
    if position[0] in range(dimension) and position[1] in range(dimension):
        return Position(position)
    return None


class Sockets(typing.TypedDict):
    north: str
    east: str
    south: str
    west: str


class Asset:
    def __init__(self, image: pygame.Surface, sockets: Sockets, name: str):
        self.image = image
        self.sockets = sockets
        self.name = name


class Tile:

    def __init__(self, position: (int, int), asset: Optional[Asset]):
        self.position = Position(position)
        self.display_position = Position(tuple(x * TILE_SIZE for x in self.position.get_position()))
        self.texture = pygame.Surface((TILE_SIZE, TILE_SIZE))
        self.asset = asset
        self.collapse = False
        neighbors = self.get_von_neuman_neighborhood()
        # print(f"{self.position} => {neighbors}")

    def set_asset(self, asset: Asset):
        self.asset = asset
        self.collapse = True

    def get_von_neuman_neighborhood(self) -> dict:
        """
        Récupère les tuiles le voisinage de von neuman de la tuile
        :return: 
        """
        # North
        north_pos = (self.position.x, self.position.y - 1)
        # Est
        east_pos = (self.position.x + 1, self.position.y)
        # South
        south_pos = (self.position.x, self.position.y + 1)
        # West
        west_pos = (self.position.x - 1, self.position.y)

        return {
            Side.NORTH: clamp(TILE_NUMBER, north_pos),
            Side.EAST: clamp(TILE_NUMBER, east_pos),
            Side.SOUTH: clamp(TILE_NUMBER, south_pos),
            Side.WEST: clamp(TILE_NUMBER, west_pos)
        }

    def draw(self, screen: pygame.Surface):
        if self.asset is None:
            return

        self.texture.blit(self.asset.image, (0, 0))
        screen.blit(self.texture, self.display_position.get_position())
        pass


def preload(configuration_file: [str]) -> dict:
    with open(configuration_file) as config:
        config = json.load(config)

    out = {}
    for (filename, data) in config.items():
        image = pygame.image.load(f"assets/map0/{filename}.png")
        image_resized = pygame.transform.scale(image, (TILE_SIZE, TILE_SIZE))
        sockets: Sockets = data["sockets"]
        out[filename] = Asset(image_resized, sockets, filename)

    return out


def socket_compatible(current_socket: str, side: Side):
    inverted_socket = current_socket[::-1]
    #print(f"sibling socket: {inverted_socket}")

    def inner(asset: Asset):
        # noinspection PyTypedDict
        asset_socket = asset.sockets[str(side.value)]
        #print(f"asset: {asset.name}, socket[{side.value}]: {asset_socket}")
        if inverted_socket == asset_socket:
            #print(f"asset : {asset.name} not filtred")
            return True
        return False

    return inner


def invert_side(side: Side) -> Side:
    match side:
        case Side.NORTH:
            return Side.SOUTH
        case Side.SOUTH:
            return Side.NORTH
        case Side.EAST:
            return Side.WEST
        case Side.WEST:
            return Side.EAST


def filter_out_of_map_neighbors(neighbor: (Side, Optional[(int, int)])) -> bool:
    return not neighbor[1] is None


def collapse(tiles: typing.List[typing.List[Tile]], assets: typing.List[Asset], current_position: Position):
    current_tile = tiles[current_position.x][current_position.y]
    #print(f"current position : {current_position}")

    for neighbor in filter(filter_out_of_map_neighbors, current_tile.get_von_neuman_neighborhood().items()):
        side, position = neighbor
        tile = tiles[position.x][position.y]
        print(f"{tile.position} | {side}")

        print("--------------")

        if tile.collapse:
            continue

        # On recherche toutes les tuiles voisines qui sont déjà fixées
        possible_assets = assets
        for sibling in filter(filter_out_of_map_neighbors, tile.get_von_neuman_neighborhood().items()):
            sibling_side, sibling_position = sibling
            sibling_tile = tiles[sibling_position.x][sibling_position.y]

            if not sibling_tile.collapse:
                continue

            inverted_side = invert_side(side)
            # noinspection PyTypedDict
            socket = sibling_tile.asset.sockets[side.value]

            possible_assets = list(filter(socket_compatible(socket, inverted_side), possible_assets))

        print(len(possible_assets))
        if not len(possible_assets):
            continue
        tile.set_asset(random.choice(possible_assets))

        print("#####################")
        collapse(tiles, assets, position)


def update(tiles: typing.List[typing.List[Tile]], screen: pygame.Surface):
    for row in tiles:
        for tile in row:
            tile.draw(screen)


def main():
    # random.seed(12)
    pygame.init()

    screen = pygame.display.set_mode(WINDOW_DIMENSIONS)
    assets = preload("assets/map0/sockets.json")

    tiles = []
    for pos_x in range(TILE_NUMBER):
        tiles.append([])
        for pos_y in range(TILE_NUMBER):
            tiles[pos_x].append(Tile((pos_x, pos_y), None))

    # effondrement de l'entropie de tile (0,0)
    current_tile = tiles[0][0]
    asset = random.choice(list(assets.values()))
    current_tile.set_asset(asset)

    collapse(
        tiles,
        list(assets.values()),
        Position((0, 0))
    )

    while True:
        update(tiles, screen)
        if pygame.event.get(pygame.QUIT):
            break
        pygame.display.update()
    pygame.quit()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()


# See PyCharm help at https://www.jetbrains.com/help/pycharm/

class TestUtils(unittest.TestCase):
    def test_clamp(self):
        self.assertEqual(Position((0, 1)), clamp(3, (0, 1)))
        self.assertEqual(None, clamp(3, (-1, 1)))
        self.assertEqual(None, clamp(3, (4, 1)))
        self.assertEqual(None, clamp(3, (2, -1)))
        self.assertEqual(None, clamp(3, (2, -1)))
        self.assertEqual(None, clamp(3, (2, 4)))
